extends Node

var skill_data

func _ready():
	var skill_file = File.new()
	skill_file.open("res://_DATA/skills.json", File.READ)
	var skill_json = JSON.parse(skill_file.get_as_text())
	skill_file.close()
	skill_data = skill_json.result.skills
	print(skill_data)
