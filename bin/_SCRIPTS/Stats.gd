extends Node

var health = 100.0
var max_health = 100.0
signal health_changed
signal death

var energy = 50.0
var max_energy = 50.0
signal energy_changed

var zapas = 320
var max_zapas = 320
signal zapas_changed

onready var ZAPAS_FOR_HEALTH = 3
onready var ZAPAS_FOR_ENERGY = 1

var move_speed = 200.0
var attack_speed = 100.0
var damage = 5.00

var STAT_STR = 10
var STAT_AGI = 10
var STAT_INT = 10
var STAT_CHA = 10

var current_skills = ["Fire_charge", "Fireball", "Explosion", "Zap"]

func mod_energy(value):
	energy = max(0, min(energy + value, max_energy))
	emit_signal("energy_changed", energy, max_energy)
	
func mod_health(value):
	health = max(0, min(health + value, max_health))
	emit_signal("health_changed", health, max_health)
	if health == 0 : emit_signal("death")

func mod_zapas(value):
	zapas = max(0, min(zapas + value, max_zapas))
	emit_signal("zapas_changed", zapas, max_zapas)

func zapas_regen_health(value):
	if zapas > 0 and health < max_health:
		var waste = min(min(value*ZAPAS_FOR_HEALTH, zapas), (max_health - health)*ZAPAS_FOR_HEALTH)
		mod_health(waste/ZAPAS_FOR_HEALTH)
		mod_zapas(-waste)
	
func zapas_regen_energy(value):
	if zapas > 0 and energy < max_energy:
		var waste = min(min(value*ZAPAS_FOR_ENERGY, zapas), (max_energy - energy)*ZAPAS_FOR_ENERGY)
		mod_energy(waste/ZAPAS_FOR_ENERGY)
		mod_zapas(-waste)

func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
