extends RigidBody2D

var skill_name
var life_time
var cooldown

var SPD # Projectile speed
var DMG # Damage


func _ready():
	life_time = IMPORT.skill_data[skill_name].lifetime
	cooldown = IMPORT.skill_data[skill_name].cooldown
	
	DMG = IMPORT.skill_data[skill_name].damage
	SPD = IMPORT.skill_data[skill_name].speed
	get_node("CollisionShape2D").get_shape().radius = IMPORT.skill_data[skill_name].radius
	
	var texture = load("res://_ASSETS/Skills/Projectiles/" + skill_name + ".png")
	$Sprite.set_texture(texture)
	
	apply_impulse(Vector2(), iso_norm(Vector2(1, 0).rotated(rotation)) * SPD)
	decompose()

func decompose():
	yield(get_tree().create_timer(life_time), "timeout")
	queue_free()

func _on_BasicAttack_body_entered(body):
	get_node("CollisionShape2D").set_deferred("disabled", true)
	if body.is_in_group("enemies"):
		body.hit(DMG)
	self.hide()
	
func iso_norm(vector):
	vector.x /= 2
	vector = vector.normalized()
	vector.x *= 2
	return vector
