extends Area2D

var skill_name
var life_time
var cooldown
var animation = "explosion"

var DMG
var RAD
var DDT

func _ready():
	life_time = IMPORT.skill_data[skill_name].lifetime
	cooldown = IMPORT.skill_data[skill_name].cooldown
	
	DMG = IMPORT.skill_data[skill_name].damage
	DDT = IMPORT.skill_data[skill_name].time
	get_node("CollisionShape2D").get_shape().radius = IMPORT.skill_data[skill_name].radius
	
	AOEAttack()
	
func AOEAttack():
	get_node("AnimationPlayer").play(animation)
	yield(get_tree().create_timer(DDT), "timeout")
	var targets = get_overlapping_bodies()
	for target in targets:
		target.hit(DMG)
	yield(get_tree().create_timer(life_time), "timeout")
	self.queue_free()