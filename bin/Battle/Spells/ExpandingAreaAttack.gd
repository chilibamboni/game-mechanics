extends Area2D

var skill_name
var life_time
var cooldown
var animation = "Zap"

var shape = preload("res://Misc/circleshape.tres")

var DMG
var RAD
var DDT

var damaged_targets = []

func _ready():
	life_time = IMPORT.skill_data[skill_name].lifetime
	cooldown = IMPORT.skill_data[skill_name].cooldown	
	
	DMG = IMPORT.skill_data[skill_name].damage
	DDT = IMPORT.skill_data[skill_name].time
	RAD = IMPORT.skill_data[skill_name].radius
	
	var texture = load("res://_ASSETS/Skills/Projectiles/" + skill_name + ".png")
	$Sprite.set_texture(texture)
	
	#get_node("CollisionShape2D").get_shape().radius = IMPORT.skill_data[skill_name].radius
	
	AOEAttack()
	
func AOEAttack():
	get_node("AnimationPlayer").play(animation)
	var step = RAD / (DDT / 0.05)
	while $CollisionShape2D.get_shape().radius <= RAD:
		var s = shape.duplicate()
		s.set_radius($CollisionShape2D.get_shape().radius + step)
		$CollisionShape2D.set_shape(s)
		var targets = get_overlapping_bodies()
		for target in targets:
			if damaged_targets.has(target):
				continue
			else:
				target.hit(DMG)
				damaged_targets.append(target)
		yield(get_tree().create_timer(0.05),"timeout")
	yield(get_tree().create_timer(life_time), "timeout")
	$CollisionShape2D.get_shape().set_radius(0)
	self.queue_free()