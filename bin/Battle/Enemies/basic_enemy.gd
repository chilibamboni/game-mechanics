extends KinematicBody2D

var max_hp = 30
var current_hp


func _ready():
	current_hp = max_hp
	$HP_bar.value = 100

func hit(damage):
	print("ОЙ")
	current_hp -= damage
	$HP_bar.value = int(current_hp*100 / max_hp)	
	if current_hp <= 0:
		death()
		
func death():
	$HP_bar.hide()
	$collision.set_deferred("disabled", true)
	hide()