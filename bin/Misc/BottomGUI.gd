extends Control

var loaded_skills = {"Skill1": "Fire_charge", "Skill2": "Fireball", "Skill3": "Explosion", "Skill4": "Zap"}

func _ready():
	LoadSkills()
	for button in get_tree().get_nodes_in_group("SkillButtons"):
		button.connect("pressed", self, "SkillPressed", [button.get_parent().get_name()])

func LoadSkills():
	for skill in loaded_skills:
		var icon = load("res://_ASSETS/Skills/icons/" + loaded_skills[skill] + ".png")
		$VBoxContainer/SkillContainers.get_node(skill + "/TextureRect").set_texture(icon)

func SkillPressed(skill):
	if loaded_skills[skill]:
		for button in $VBoxContainer/SkillContainers.get_children():
			button.get_node("TextureSelect").set_modulate("000000")
		$VBoxContainer/SkillContainers.get_node(skill + "/TextureSelect").set_modulate("0eff00")
		get_parent().get_parent().get_parent().get_parent().selected_skill = loaded_skills[skill]


func _on_Stats_energy_changed(e, m):
	$VBoxContainer/PB/TextureProgress.set_max(m)
	$VBoxContainer/PB/TextureProgress.set_value(e)
	$VBoxContainer/PB/Label.text = str(max(floor(e), 0)) + " / " + str(m)
	

func _on_Stats_zapas_changed(e, m):
	$VBoxContainer/PB/zapas.set_max(m)
	$VBoxContainer/PB/zapas.set_value(e)
