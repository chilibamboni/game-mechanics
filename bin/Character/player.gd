extends KinematicBody2D

var velocity = Vector2()
var direction_vector = 0
var direction = 2
var last_direction = Vector2()
var run = 0

var regen_tick = 0
onready var REGEN_PAUSE = 1
onready var REGEN_TICK_TIME = 0.25

var selected_skill = "Fire_charge"
onready var basic_attack = preload("res://Battle/Spells/BasicAttack.tscn")
onready var area_attack = preload("res://Battle/Spells/AreaAttack.tscn")
onready var expanding_area_attack = preload("res://Battle/Spells/ExpandingAreaAttack.tscn")


var attack_ready = true
var attack_speed_mul = 1.0

func _process(delta):
	get_using_skills()
	pass

func _physics_process(delta):
	get_movement()
	if velocity.length() > 0 : mod_running()
	move_and_collide(velocity * delta)
	if run:
		$Stats.mod_energy(-5.0*delta)
	regenerate(delta)

func get_using_skills():
	var waste = IMPORT.skill_data[selected_skill].energy
	if Input.is_action_pressed("shoot_a") and waste <= $Stats.energy and attack_ready:
		attack_ready = false
		var spell
		$Stats.mod_energy(-waste)
		regen_tick = -REGEN_PAUSE
		get_node("TurningPoint").rotation = get_angle_to(get_global_mouse_position())
		
		match IMPORT.skill_data[selected_skill].SkillType:
			"BasicAttack":
				spell = basic_attack.instance()
				spell.skill_name = selected_skill
				spell.rotation = get_angle_to(get_global_mouse_position())
				spell.position = get_node("TurningPoint/CastPoint").get_global_position()
				get_parent().add_child(spell)
				
			"AreaAttack":
				spell = area_attack.instance()
				spell.skill_name = selected_skill
				spell.position = get_global_mouse_position()
				get_parent().add_child(spell)
				
			"ExpandingAreaAttack":
				spell = expanding_area_attack.instance()
				spell.skill_name = selected_skill
				spell.position = Vector2(0,0) # get_global_position()
				add_child(spell)
		
		yield(get_tree().create_timer(spell.cooldown), "timeout")
		attack_ready = true

func get_movement():
	direction_vector = Vector2()
	direction_vector.x += int(Input.is_action_pressed('ui_right'))
	direction_vector.x -= int(Input.is_action_pressed('ui_left'))
	direction_vector.y += int(Input.is_action_pressed('ui_down'))
	direction_vector.y -= int(Input.is_action_pressed('ui_up'))
	direction = 5 + direction_vector.x - 3*direction_vector.y
	
	if direction_vector.length() > 0:
		velocity = direction_vector.normalized() * $Stats.move_speed
		velocity.x *= 2		
		last_direction = direction_vector
	else:
		velocity = Vector2(0,0)
		run = 0
		
	var angle = get_angle_to(get_global_mouse_position())-last_direction.angle()
	
	var t = Transform(Vector3(cos(angle),sin(angle),0),Vector3(-sin(angle),cos(angle),0),Vector3(0,0,1),Vector3(0,0,0))
	$View/Viewport/avatar.get_node("Armature").set_rotation(Vector3(0,-last_direction.angle(),0))
	$View/Viewport/avatar.get_node("Armature/Skeleton").set_bone_pose(6, t)

func mod_running():
	if Input.is_action_pressed("ui_run") and $Stats.energy > 0:
		velocity *= 1.5
		run = 1
	else:
		run = 0
	pass

func regenerate(delta):
	if not run: regen_tick += delta
	else: regen_tick = 0
	if regen_tick >= REGEN_TICK_TIME:
		regen_tick -= REGEN_TICK_TIME
		$Stats.zapas_regen_energy(2)

func hit(damage):
	print("ОЙ")
	$Stats.mod_health(-damage)
	











